/* Auth Screens */
export const SIGN_IN = 'SignIn';

/* Dashboard Screens */
export const HOME = 'Home';

/* User Screens */
export const USER_PROFILE = 'UserProfile';