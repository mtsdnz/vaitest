import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import AuthNavigator from './AuthNavigator';
import DashboardNavigator from './Dashboard/DashboardNavigator';

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      Dashboard: DashboardNavigator,
      Auth: AuthNavigator,
    },
    {
      initialRouteName: 'Auth',
    },
  ),
);

export default AppContainer;