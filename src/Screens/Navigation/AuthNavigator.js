import {
  createStackNavigator,
} from 'react-navigation';
import { SIGN_IN } from '../ScreenNames';
import SignInScreen from '../Auth/SignIn';
import headerStyle from '../../styles/header';

const navigator = createStackNavigator({
  [SIGN_IN]: {
    screen: SignInScreen,
  },
},
{
  defaultNavigationOptions: headerStyle.defaultStyle,
});

export default navigator;
