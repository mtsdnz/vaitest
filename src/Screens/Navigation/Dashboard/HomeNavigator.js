import {
  createStackNavigator,
} from 'react-navigation';
import HomeScreen from '../../Dashboard/Home';
import UserProfileScreen from '../../Dashboard/UserProfile';

import {
  HOME, USER_PROFILE,
} from '../../ScreenNames';
import headerStyle from '../../../styles/header';

const navigator = createStackNavigator(
  {
    [HOME]: { screen: HomeScreen },
    [USER_PROFILE]: { screen: UserProfileScreen },
  },
  {
    initialRouteName: HOME,
    defaultNavigationOptions: headerStyle.defaultStyle,
  },

);

export default navigator;
