import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import HomeNavigator from './HomeNavigator';
import Sidebar from '../../../Components/Sidebar/Sidebar';
import { HOME } from '../../ScreenNames';

const sidebarMenus = [{
  title: 'Inicio',
  icon: 'home',
  screenName: HOME,
},
{
  title: 'Sair',
  icon: 'arrow-circle-o-left',
  screenName: 'Auth',
},
];

const DrawerNavigator = createDrawerNavigator(
  {
    Home: HomeNavigator,
  },
  {
    contentComponent: props => (<Sidebar menus={sidebarMenus} {...props} />),
  },
);

export default DrawerNavigator;
