import React from 'react';
import SignInContainer from '../../Containers/Auth/SignIn/SignIn';
import { HOME } from '../ScreenNames';

function SignInScreen({ navigation }) {
  return (
    <SignInContainer onSignIn={() => navigation.navigate(HOME)} />
  );
}

SignInScreen.navigationOptions = {
  title: 'Login',
};

export default SignInScreen;
