import React, { Component } from 'react';
import { Alert, ActivityIndicator } from 'react-native';
import UsersService from '../../Services/UsersService';
import Constants from '../../Common/Constants';
import colors from '../../styles/colors';
import styles from '../../Components/PaginatedList/styles';
import Icon from '../../Components/Icon/Icon';
import UserProfileContainer from '../../Containers/UserProfile/UserProfile';
import header from '../../styles/header';

class UserProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {}, loading: true, error: false,
    };
  }

  componentDidMount(): void {
    this.mounted = true;
    this.fetchCurrentUser();
  }

  componentWillUnmount(): void {
    this.mounted = false;
  }

  fetchCurrentUser = () => {
    const { navigation } = this.props;

    this.setState({ loading: true, error: false });
    // Em um ambiente real, nós receberiamos o id do usuário como parâmetro, e fariamos fetch na API
    // Porém, não é possível fazer isso com o randomAPI, então faremos assim.
    const user = navigation.getParam('user', {});
    UsersService
      .getUsers()
      .page(user.page) // Número da página do usuário
      .limit(Constants.usersPerPage) // Quantidade de itens por página
      .fetch()
      .then(data => this.onUserFetchSuccess(data, user))
      .catch(this.onUserFetchError)
      .finally(this.onUserFetchDone);
  };

  onUserFetchSuccess= (data, user) => {
    const fetchedUser = data.results[user.index];

    this.safeSetState({ user: fetchedUser });
  };

  onUserFetchError = (err) => {
    console.error(err);
    this.safeSetState({ error: true });
  };

  onUserFetchDone = () => {
    this.safeSetState({ loading: false });
  };

  safeSetState = (state) => {
    if (this.mounted) this.setState(state);
  };


  render() {
    const { loading, user, error } = this.state;

    if (error) {
      return (
        <Icon
          name="undo"
          size={40}
          color={colors.primary}
          onPress={this.fetchCurrentUser}
          style={styles.retryIcon}
        />
      );
    }

    if (loading) return <ActivityIndicator color={colors.primary} size="large" />;


    return (
      <UserProfileContainer user={user} />
    );
  }
}

function showDeleteUserAlert(navigation) {
  const user = navigation.getParam('user', {});
  Alert.alert(
    'Deletar Usuário',
    `O usuário ${user.email} será permanentemente deletado.`,
    [
      // Em um ambiente real, nós disparariamos a ação de deletar o usuário aqui
      { text: 'OK', onPress: () => navigation.goBack() },
      { text: 'Cancelar', style: 'cancel' },
    ],
    { cancelable: true },
  );
}

UserProfileScreen.navigationOptions = ({ navigation }) => ({
  headerStyle: header.headerTransparent,
  title: 'Perfil',
  headerRight: <Icon
    name="user-times"
    color={colors.header.button}
    size={25}
    style={{ marginRight: 18 }}
    onPress={() => showDeleteUserAlert(navigation)}
  />,
});

UserProfileScreen.propTypes = {};

export default UserProfileScreen;
