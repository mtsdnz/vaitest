import React, { Fragment } from 'react';
import MenuIcon from '../../Components/MenuIcon/MenuIcon';
import HomeContainer from '../../Containers/Home/Home';
import { USER_PROFILE } from '../ScreenNames';

function HomeScreen({ navigation }) {
  return (
    <Fragment>
      <HomeContainer onViewUser={user => navigation.navigate(USER_PROFILE, { user })} />
    </Fragment>
  );
}

HomeScreen.navigationOptions = {
  title: 'Dashboard',
  headerLeft: <MenuIcon />,
};
HomeScreen.propTypes = {};

export default HomeScreen;
