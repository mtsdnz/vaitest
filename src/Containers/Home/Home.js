import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';
import UsersService from '../../Services/UsersService';
import UserList from '../../Components/UserList/UserList';
import { parseName } from '../../Utils/UserUtils';
import Constants from '../../Common/Constants';

class HomeContainer extends Component {
  constructor(props) {
    super(props);

    this.usersLength = 0;
    this.itemsPerPage = Constants.usersPerPage;
    this.state = { users: [], isFetching: true, error: false };
  }

  componentDidMount() {
    this.mounted = true;
    this.fetchUsers(1);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  // Seta o state apenas se o componente estiver montado.
  // Deve ser utilizado dentro de operações asyncronas.
  safeSetState = (state) => {
    if (this.mounted) this.setState(state);
  };

  fetchUsers = (page) => {
    this.safeSetState({ isFetching: true, error: false });
    UsersService
      .getUsers()
      .withFields(['name', 'picture', 'gender', 'email', 'phone', 'location']) // especifica quais fields da API nós utilizaremos
      .page(page) // Número da página
      .limit(this.itemsPerPage) // Quantidade de itens por página
      .fetch()
      .then(this.parseApiData)
      .catch(this.onFetchUsersError)
      .finally(this.onFetchUsersDone);
  };

  onFetchUsersDone = () => {
    this.safeSetState({ isFetching: false });
  };

  onFetchUsersError = () => {
    this.safeSetState({ error: true });
  };

  parseApiData = (data) => {
    // checa se os dados obtidos não são nulos, e se o resultado é um array.
    if (data && Array.isArray(data.results)) {
      const { results, info } = data;
      const { page } = info;
      // converte os dados recebidos
      const parsedUsers = this.parseApiUsers(page, results);
      this.usersLength = this.itemsPerPage * page;
      this.safeSetState({
        // se estivermos na página 1, os itens são 'resetados', se não, nós concatenamos os 2 arrays
        users: page === 1 ? parsedUsers : _.concat(this.state.users, parsedUsers),
      });
    }
  };

  /* Converte o array the usuários recebidos da API */
  parseApiUsers = (page, users) => users.map(({
    name, picture, gender, email, phone, location,
  }, i) => (
    {
      page,
      index: i,
      name: parseName(name),
      email,
      phone,
      address: `${location.city}, ${location.state}`,
      image: picture.medium,
      isMale: gender === 'male',
    }));


  render() {
    const { onViewUser } = this.props;
    const { users, isFetching, error } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Text>{`Usuários carregados: ${this.usersLength}`}</Text>
        <UserList
          onViewUser={onViewUser}
          users={users}
          error={error}
          pageChanged={p => this.fetchUsers(p)}
          isFetching={isFetching}
        />

      </View>
    );
  }
}

HomeContainer.propTypes = {
  onViewUser: PropTypes.func.isRequired,
};

export default HomeContainer;
