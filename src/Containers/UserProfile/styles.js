import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../styles/colors';

const { height } = Dimensions.get('window');
const vh = height / 100;

const avatarSize = vh * 30 * 0.5;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    height: '40%',
  },
  avatarEditIconContainer: {
    backgroundColor: colors.primary,
    right: 0,
    top: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    borderColor: 'white',
    borderWidth: 1,
  },
  avatarEditIcon: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
  },
  avatar: {
    width: avatarSize,
    height: avatarSize,
    borderRadius: avatarSize / 2,
    overflow: 'hidden',
    borderWidth: 1.5,
    borderColor: 'white',
  },
  name: {
    fontSize: 23,
    marginTop: 10,
    color: colors.text.light,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  headerBottom: {
    position: 'absolute',
    bottom: 0,
    width: '95%',
  },
  tabIcon: {
    fontSize: 23,
    color: colors.text.light,
  },
  rowContainer: {
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  actionsContainer: {
    marginTop: 15,
    flexDirection: 'row',
    alignContent: 'stretch',
    width: '100%',
  },
  actionButton: {
    flex: 1,
    margin: 15,
  },
  content: {
    margin: 10,
  },
  nameInputsContainer: {
    flexDirection: 'row',
  },
  flexInput: {
    flex: 1,
  },
  genderCheckbox: {
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  genderContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  genderIcon: {
    fontSize: 26,
    marginLeft: 5,
    marginRight: 15,
  },
});
