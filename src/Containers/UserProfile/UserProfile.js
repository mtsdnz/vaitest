import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import styles from './styles';
import Icon from '../../Components/Icon/Icon';
import Tabs from '../../Components/Tabs/Tabs';
import Tab from '../../Components/Tabs/Tab/Tab';
import defaultProfileImage from '../../assets/images/person.png';
import { parseName } from '../../Utils/UserUtils';
import FloatingLabelInput from '../../Components/FloatingLabelInput/FloatingLabelInput';
import ImagePickerHandler from '../../Components/ImagePickerHandler/ImagePickerHandler';
import CircleCheckbox from '../../Components/CircleCheckBox/CircleChebox';
import CustomButton from '../../Components/Button/Button';
import { showToastOrAlert } from '../../Utils/alertUtils';
import GenderIcon from '../../Components/GenderIcon/GenderIcon';

class UserProfileContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 0,
      photo: props.user.picture.large,
      user: _.clone(props.user),
    };
  }

  onTabChange = (tab) => {
    this.setState({ activeTab: tab });
  };

  onPhotoChanged = (photo) => {
    this.setState({ photo });
  };

  onUserChange = (prop, value) => {
    const { user } = this.state;

    _.set(user, prop, value);
    this.setState({ user });
  };

  UserInput = ({ path, ...rest }) => {
    const { user } = this.state;

    return (
      <FloatingLabelInput
        {...rest}
        value={_.get(user, path).toString()}
        onChangeText={v => this.onUserChange(path, v)}
      />
    );
  };

  renderUserInformations = () => {
    const { user } = this.state;
    const { dob, gender } = user;

    const { UserInput } = this;

    return (
      <View>
        <View style={styles.nameInputsContainer}>
          <UserInput
            placeholder="Titulo"
            textContentType="namePrefix"
            path="name.title"
            style={{ width: '20%', marginRight: 15 }}
            maxLength={4}
          />
          <UserInput
            placeholder="Nome"
            autoCompleteType="name"
            textContentType="name"
            path="name.first"
            maxLength={15}
            style={{ width: '75%' }}
          />
        </View>

        <UserInput
          placeholder="Sobrenome"
          textContentType="middleName"
          maxLength={15}
          path="name.last"
        />

        <View style={styles.rowContainer}>
          <UserInput
            placeholder="Telefone"
            maxLength={16}
            autoCompleteType="tel"
            keyboardType="numeric"
            textContentType="telephoneNumber"
            style={styles.flexInput}
            path="phone"
          />
          <UserInput
            placeholder="Celular"
            keyboardType="numeric"
            autoCompleteType="tel"
            textContentType="telephoneNumber"
            maxLength={16}
            style={styles.flexInput}
            path="cell"
          />
        </View>
        <View>
          <UserInput
            placeholder="Nascimento"
            editable={false}
            selectTextOnFocus={false}
            path="dob.date"
          />
          <DatePicker
            androidMode="spinner"
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
            }}
            date={dob.date}
            mode="date"
            maxDate={moment().format('YYYY-MM-DD')}
            format="YYYY-MM-DD"
            confirmBtnText="Confirmar"
            cancelBtnText="Cancelar"
            showIcon={false}
            customStyles={{
              dateInput: {
                marginTop: 15,
                display: 'none',
              },
            }}
            onDateChange={(date) => { this.onUserChange('dob.date', date); }}
          />
        </View>

        <View style={styles.genderContainer}>
          <CircleCheckbox
            checked={gender === 'male'}
            onPress={() => this.onUserChange('gender', 'male')}
            style={styles.genderCheckbox}
          >
            <GenderIcon isMale style={styles.genderIcon} />
          </CircleCheckbox>

          <CircleCheckbox
            checked={gender === 'female'}
            onPress={() => this.onUserChange('gender', 'female')}
            style={styles.genderCheckbox}
          >
            <GenderIcon isMale={false} style={styles.genderIcon} />
          </CircleCheckbox>
        </View>
      </View>
    );
  };

  renderUserPrivateInformations = () => {
    const { UserInput } = this;

    return (
      <View>
        <UserInput
          placeholder="E-mail"
          keyboardType="email-address"
          textContentType="emailAddress"
          autoCompleteType="email"
          path="email"
        />

        <UserInput
          placeholder="Login"
          textContentType="username"
          autoCompleteType="username"
          path="login.username"
        />

        <UserInput
          placeholder="Senha"
          autoCompleteType="password"
          path="login.password"
          secureTextEntry
        />

      </View>
    );
  };

  renderUserLocationInformations = () => {
    const { UserInput } = this;

    return (
      <View>
        <UserInput
          placeholder="Endereço"
          autoCompleteType="street-address"
          textContentType="fullStreetAddress"
          path="location.street"
        />

        <View style={styles.rowContainer}>
          <UserInput
            placeholder="Cidade"
            textContentType="addressCity"
            style={styles.flexInput}
            path="location.city"
          />

          <UserInput
            placeholder="Estado"
            textContentType="addressState"
            style={styles.flexInput}
            path="location.state"
          />
        </View>

        <UserInput
          placeholder="CEP"
          textContentType="postalCode"
          keyboardType="numeric"
          path="location.postcode"
        />

        <View style={styles.rowContainer}>
          <UserInput
            placeholder="Latitude"
            keyboardType="numeric"
            style={styles.flexInput}
            path="location.coordinates.latitude"
          />
          <UserInput
            placeholder="Longitude"
            keyboardType="numeric"
            style={styles.flexInput}
            path="location.coordinates.longitude"
          />
        </View>

      </View>
    );
  };

  onSave = () => {
    // Aqui nós chamariamos a action para salvar o usuário.
    showToastOrAlert('Sucesso', 'Usuário salvo com sucesso!');
  };

  render() {
    const { activeTab, user, photo } = this.state;
    const { name } = user;

    console.log(user);
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.avatarContainer}>
            <ImagePickerHandler photo={photo} onPhotoChanged={this.onPhotoChanged}>
              <Image style={styles.avatar} source={photo ? { uri: photo } : defaultProfileImage} />
            </ImagePickerHandler>
            <View style={styles.avatarEditIconContainer}>
              <Icon name="mode-edit" type="MaterialIcons" size={20} style={styles.avatarEditIcon} />
            </View>
          </View>
          <Text style={styles.name}>{parseName(name, true)}</Text>

          <View style={styles.headerBottom}>
            <Tabs value={activeTab} onChange={this.onTabChange}>
              <Tab>
                <Icon name="user" style={styles.tabIcon} />
              </Tab>
              <Tab>
                <Icon name="globe" style={styles.tabIcon} />
              </Tab>
              <Tab>
                <Icon name="security" type="MaterialIcons" style={styles.tabIcon} />
              </Tab>
            </Tabs>
          </View>
        </View>

        <View style={styles.content}>
          {activeTab === 0 && this.renderUserInformations()}
          {activeTab === 1 && this.renderUserLocationInformations()}
          {activeTab === 2 && this.renderUserPrivateInformations()}
          <View style={styles.actionsContainer}>
            <CustomButton
              style={styles.actionButton}
              title="Salvar"
              onPress={this.onSave}
            />
          </View>
        </View>
      </View>
    );
  }
}

UserProfileContainer.propTypes = {
  user: PropTypes.object.isRequired,
};

export default UserProfileContainer;
