import React, { useState } from 'react';
import { Picker, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from './styles';
import validateForm from './validation';
import Input from '../../../Components/Input/Input';
import Button from '../../../Components/Button/Button';
import localization from '../../../Localization';

const orderedCountries = _.orderBy(localization.availableCountries, c => c.name);

function SignInContainer(props) {
  const { onSignIn } = props;

  const [language, setLanguage] = useState(localization.currentCountryCode);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});

  function onSubmit() {
    const validationErrors = validateForm(email, password);
    // caso não tenha erros, vai para a próxima tela.
    // em um ambiente real, nós chamariamos a API de autenticação aqui.
    if (!validationErrors) {
      onSignIn();
    } else {
      setErrors(validationErrors);
    }
  }

  function onLanguageChange(languageCode) {
    setLanguage(languageCode);
    localization.currentCountryCode = languageCode;
  }

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.title}>Bem-vindo ao Dashboard</Text>
        <Text style={styles.subTitle}>
Faça login para começar a usar todas as funções do
          aplicativo!
        </Text>
      </View>

      <View style={styles.formContainer}>
        <Input
          returnKeyType="next"
          placeholder="E-mail"
          keyboardType="email-address"
          textContentType="emailAddress"
          autoCompleteType="email"
          error={errors.email}
          blurOnSubmit={false}
          value={email}
          onChangeText={setEmail}
          onSubmitEditing={() => this.passwordInput.focus()}
        />
        <Input
          placeholder="Senha"
          ref={i => this.passwordInput = i}
          textContentType="password"
          autoCompleteType="password"
          error={errors.password}
          secureTextEntry
          returnKeyType="done"
          blurOnSubmit={false}
          value={password}
          onChangeText={setPassword}
          onSubmitEditing={onSubmit}
        />
        <View style={styles.submitButtonWrapper}>
          <Button
            title="Logar"
            accessibilityLabel="Logar"
            onPress={onSubmit}
          />
        </View>
      </View>
      <View style={styles.countryContainer}>
        <Text style={styles.countryLabel}>País </Text>
        <Picker
          style={styles.countryPicker}
          selectedValue={language}
          onValueChange={onLanguageChange}
        >
          {orderedCountries.map(
            ({ code, name }) => <Picker.Item key={code} label={name} value={code} />,
          )}
        </Picker>
      </View>
    </View>
  );
}

SignInContainer.propTypes = {
  onSignIn: PropTypes.func.isRequired,

};

export default SignInContainer;
