import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  textContainer: {
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: colors.text.dark,
    fontSize: 20,
  },
  subTitle: {
    color: colors.text.primary,
    fontSize: 16,
    textAlign: 'center',
  },
  formContainer: {
    width: '90%',
  },
  submitButtonWrapper: {
    marginTop: 20,
  },
  countryContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  countryPicker: {
    height: 50,
    width: 200,
  },
  countryLabel: {
    color: colors.text.primary,
  },

});
