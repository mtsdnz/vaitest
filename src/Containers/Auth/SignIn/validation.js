import validate from 'validate.js';
import { parseErrors } from '../../../Utils/validationUtils';

const constraints = {
  email: {
    presence: true,
    email: {
      message: 'Por favor, insira um e-mail válido.',
    },
  },
  password: {
    presence: true,
    length: {
      minimum: 1,
      message: 'Por favor, informe sua senha',
    },
  },
};

/*
  Valida o e-mail, e o password inserido pelo usuário.
  Se houver erros, retornará um objeto com os errors, se não, undefined.
 */
const validateForm = (email, password) => (parseErrors(validate({ email, password },
  constraints, { fullMessages: false })));

export default validateForm;
