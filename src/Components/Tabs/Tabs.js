import React, { Component } from 'react';
import {
  View, Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

class Tabs extends Component {
  constructor(props) {
    super(props);

    this.tabsPositions = [];
    this.state = {
      linePosition: new Animated.Value(0),
    };
  }

  shouldComponentUpdate(nextProps) {
    return this.props.value !== nextProps.value;
  }

  onLayout = (event, idx) => {
    this.tabsPositions[idx] = event.nativeEvent.layout.x;
  };

  onTabClick = (idx) => {
    const { value, onChange } = this.props;
    if (value === idx) return;

    this.animateLine(this.tabsPositions[idx]);
    onChange(idx);
  };

  animateLine = (position) => {
    const { linePosition } = this.state;
    const { duration } = this.props;

    Animated.spring(linePosition, {
      toValue: position,
      duration,
    }).start();
  };

  render() {
    const { children, bottomLineColor } = this.props;
    const { linePosition } = this.state;
    return (
      <View style={styles.tabs}>
        {
          React.Children.map(children, (child, i) => React.cloneElement(child, {
            onLayout: event => this.onLayout(event, i),
            onPress: () => this.onTabClick(i),
          }))
        }
        <Animated.View
          style={[styles.tabBottomLine,
            {
              left: linePosition,
              width: `${100 / children.length}%`,
              borderBottomColor: bottomLineColor,
            }]}
        />
      </View>
    );
  }
}
Tabs.defaultProps = {
  duration: 100,
  bottomLineColor: 'white',
};

Tabs.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  children: PropTypes.array.isRequired,
  // a cor da linha que fica em baixo da tab
  bottomLineColor: PropTypes.string,
  // Animation duration
  duration: PropTypes.number,
};

export default Tabs;
