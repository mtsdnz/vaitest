import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  tabs: {
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  tabBottomLine: {
    borderBottomWidth: 5,
    position: 'absolute',
    bottom: 1,
  },
});
