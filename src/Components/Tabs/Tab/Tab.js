import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const Tab = ({
  text, textStyle, style, children, ...rest
}) => (
  <TouchableOpacity
    style={[styles.tab, style]}
    {...rest}
  >
    {text && <Text style={[styles.tabText, textStyle]}>{text}</Text>}
    {children}
  </TouchableOpacity>
);

Tab.propTypes = {
  text: PropTypes.string,
  textStyle: PropTypes.object,
  style: PropTypes.object,
};

export default Tab;
