import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './styles';
import GenderIcon from '../GenderIcon/GenderIcon';

class UserCard extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext): boolean {
    return this.props.user.name !== nextProps.user.name;
  }

  render() {
    const { user, onPress } = this.props;

    const {
      image, name, email, phone, address, isMale,
    } = user;

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          if (typeof onPress === 'function') onPress(user);
        }}
      >
        <View style={styles.avatarContainer}>
          <FastImage
            style={styles.avatar}
            source={{
              uri: image,
              priority: FastImage.priority.low,
            }}
          />
        </View>

        <View style={styles.userContent}>
          <View style={styles.basicInfoContent}>
            <Text style={styles.name}>{name}</Text>
            {email && <Text style={styles.textLight}>{email}</Text>}
            {phone && <Text style={styles.textLight}>{phone}</Text>}
          </View>
          {address && <Text style={styles.address}>{address}</Text>}
          {typeof isMale === 'boolean'
          && <GenderIcon isMale={isMale} size={20} style={styles.genderIcon} />}
        </View>
      </TouchableOpacity>
    );
  }
}

UserCard.propTypes = {
  user: PropTypes.shape({
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    name: PropTypes.string.isRequired,
    email: PropTypes.string,
    phone: PropTypes.string,
    address: PropTypes.string,
    isMale: PropTypes.bool,
  }).isRequired,
  onPress: PropTypes.func,
};

export default UserCard;
