import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  container: {
    height: 100,
    borderBottomColor: colors.primary,
    borderBottomWidth: 0.5,
    marginBottom: 2,
    flexDirection: 'row',
  },
  avatarContainer: {
    width: '30%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 85,
    height: 85,
    borderRadius: 5,
  },
  userContent: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 10,
  },
  basicInfoContent: {
    width: '88%',
  },
  address: {
    position: 'absolute',
    bottom: 1,
    right: 2,
    color: colors.text.primary,
    fontSize: 13,
    textTransform: 'capitalize',
  },
  name: {
    fontSize: 16,
    textTransform: 'capitalize',
  },
  textLight: {
    color: colors.text.primary,
    lineHeight: 18,
  },
  genderIcon: {
    right: 10,
    top: 5,
    position: 'absolute',
  },
  genderIconMale: {
    color: '#0195da',
  },
  genderIconFemale: {
    color: '#804098',
  },
});
