import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

// Em um ambiente real, esse botão poderia ter mais opções de customização, mas para esse teste, vamos manter simples.
const CustomButton = ({
  title, activeOpacity, style, children, ...rest
}) => (
  <TouchableOpacity activeOpacity={activeOpacity} style={[styles.button, style]} {...rest}>
    {title && <Text style={styles.text}>{title}</Text>}
    {children}
  </TouchableOpacity>
);

CustomButton.defaultProps = {
  activeOpacity: 0.9,
};

CustomButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  activeOpacity: PropTypes.number,
};

export default CustomButton;
