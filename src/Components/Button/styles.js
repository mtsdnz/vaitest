import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  button: {
    height: 46,
    backgroundColor: colors.primary,
    marginTop: 5,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  text: {
    color: colors.text.light,
    fontWeight: 'bold',
    fontSize: 17,
  },
});
