import React, { Component } from 'react';
import { Alert, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import ImagePicker from 'react-native-image-picker';
import { showToastOrAlert } from '../../Utils/alertUtils';

class ImagePickerHandler extends Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.photo !== nextProps.photo;
  }

  onPress = () => {
    const { onPress } = this.props;

    if (onPress) {
      onPress();
    }

    this.tryChangePhoto();
  };

  tryChangePhoto = async () => {
    await this.askWhereToPickImage().then(async (result) => {
      const photoOptions = {
        cameraType: 'front',
        mediaType: 'photo',
        allowsEditing: true,
        permissionDenied: {
          title: 'Permissão negada',
          text: 'Precisamos de acesso a câmera e a sua galeria, para poder tirar fotos, e escolher imagens da sua biblioteca. ',
          reTryTitle: 'Tentar novamente',
          okTitle: 'Cancelar',
        },
      };
      switch (result) {
        case 'gallery': {
          return ImagePicker.launchImageLibrary(photoOptions, this.changePhoto);
        }
        case 'camera': {
          return ImagePicker.launchCamera(photoOptions, this.changePhoto);
        }
        default: {
          return undefined;
        }
      }
    });
  };

  changePhoto = async (result) => {
    const { onPhotoChanged } = this.props;
    if (result && result.uri && !result.didCancel) {
      showToastOrAlert('Sucesso', 'Foto atualizada!');

      if (onPhotoChanged) {
        onPhotoChanged(result.uri);
      }
    }
  };


  askWhereToPickImage = () => {
    const { photo } = this.props;
    return new Promise((resolve) => {
      const buttons = [
        { text: 'Selecionar da galeria', onPress: () => resolve('gallery') },
        { text: 'Selecionar da Câmera', onPress: () => resolve('camera') },
      ];

      // if the user has a photo, add the remove button.
      if (photo) {
        buttons.push({
          text: 'Remover Foto',
          onPress: () => {
            this.askToRemovePhoto();
            resolve('remove');
          },
          style: 'cancel',
        });
      }

      Alert.alert(
        'Atualizar Foto',
        'De onde deseja escolher a foto?',
        buttons,
        {
          cancelable: true,
        },
      );
    });
  };

  askToRemovePhoto = () => {
    Alert.alert(
      'Remover Foto',
      'Deseja remover essa foto?',
      [
        {
          text: 'Cancelar', style: 'cancel',
        },
        {
          text: 'Sim',
          onPress: () => {
            this.removeProfilePhoto();
          },
        },
      ],
      { cancelable: false },
    );
  };

  removeProfilePhoto = () => {
    const { onPhotoChanged } = this.props;

    if (onPhotoChanged) {
      onPhotoChanged(undefined);
    }

    showToastOrAlert('Sucesso', 'Foto removida');
  };

  render() {
    const { children } = this.props;
    return (
      <TouchableOpacity onPress={this.onPress}>
        {children}
      </TouchableOpacity>
    );
  }
}


ImagePickerHandler.propTypes = {
  onPhotoChanged: PropTypes.func.isRequired,
  photo: PropTypes.string,
  onPress: PropTypes.func,
};

export default ImagePickerHandler;
