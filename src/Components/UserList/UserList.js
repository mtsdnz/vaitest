import React from 'react';
import {
  View, FlatList, Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import PaginatedListView from '../PaginatedList/PaginatedList';
import UserCard from '../UserCard/UserCard';

const AnimatedListView = Animated.createAnimatedComponent(FlatList);

const UserList = ({
  pageChanged, isFetching, canFetchMore, error, users, renderUser, itemHeight, onViewUser,
}) => (
  <View style={styles.container}>
    <PaginatedListView
      pageChanged={pageChanged}
      isFetching={isFetching}
      canFetchMore={canFetchMore}
      error={error}
      items={users}
      endReachedMessage="Não há mais usuários"
      emptyMessage="Não há usuários."
    >
      {({
        refresh, renderFooter, refreshing, items, endReached,
      }) => (
        <AnimatedListView
          data={items}
          // Essa opção fará com que os componentes fora do campo de visão sejam desmontados, reduzindo o consumo de memoria.
          removeClippedSubviews
          // Especificando a altura de cada item manualmente, faz com que o FlatList não tenha que calcular dinamicamente sempre que a quantidade de itens mudar, assim a perfomance fica bem melhor.
          getItemLayout={(data, index) => ({
            length: itemHeight,
            offset: itemHeight * index,
            index,
          })}
          keyExtractor={(item, index) => `user-${item.email}-${index}`}
          renderItem={typeof renderUser === 'function' ? renderUser
            : ({ item, index }) => <UserCard user={item} onPress={onViewUser} />}
          onRefresh={refresh}
          refreshing={refreshing}
          ListFooterComponent={renderFooter}
          onEndReachedThreshold={0.15}
          onEndReached={endReached}
          scrollEventThrottle={1}
        />
      )}
    </PaginatedListView>
  </View>
);

UserList.defaultProps = {
  canFetchMore: true,
  isFetching: false,
  error: false,
  itemHeight: 100,
};

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    name: PropTypes.string.isRequired,
    email: PropTypes.string,
    phone: PropTypes.string,
    address: PropTypes.string,
    isMale: PropTypes.bool,
  })).isRequired,
  onViewUser: PropTypes.func.isRequired,
  pageChanged: PropTypes.func.isRequired,
  // O tamanho de cada item na lista. Padrão é 100.
  itemHeight: PropTypes.number,
  renderUser: PropTypes.func,
  canFetchMore: PropTypes.bool,
  isFetching: PropTypes.bool,
  error: PropTypes.bool,
};

export default UserList;
