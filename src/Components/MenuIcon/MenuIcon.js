import React from 'react';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import styles from './styles';
import Icon from '../Icon/Icon';

const MenuIcon = ({
  navigation, name, size, ...rest
}) => (
  <Icon
    name={name}
    style={styles.headerIcon}
    size={size}
    onPress={navigation.openDrawer}
    {...rest}
  />
);

MenuIcon.defaultProps = {
  name: 'bars',
  size: 25,
};

MenuIcon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.number,
  navigation: PropTypes.shape(
    {
      openDrawer: PropTypes.func.isRequired,
    },
  ).isRequired,
};

export default withNavigation(MenuIcon);
