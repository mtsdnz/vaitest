import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  headerIcon: {
    marginRight: 15,
    marginLeft: 15,
    color: colors.header.button,
  },
});
