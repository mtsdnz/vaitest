import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  input: {
    height: 45,
    color: colors.primary,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    fontSize: 16,
    paddingHorizontal: 10,
    marginBottom: 5,
    marginTop: 5,
  },
  error: {
    borderColor: 'red',
  },
  textError: {
    marginTop: -5,
    marginBottom: 5,
    marginLeft: 4,
    color: 'red',
  },


});
