import React, { Fragment } from 'react';
import { TextInput, Text } from 'react-native';
import PropTypes from 'prop-types';
import colors from '../../styles/colors';
import styles from './styles';

// Em um ambiente real, esse input poderia ter mais opções de customização, mas para esse teste, vamos manter simples.
const Input = React.forwardRef(({ style, error, ...rest }, ref) => (
  <Fragment>
    <TextInput
      ref={ref}
      style={[styles.input, error && styles.error, style]}
      placeholderTextColor={colors.primary}
      {...rest}
    />
    {typeof error === 'string' && error && <Text style={styles.textError}>{error}</Text>}
  </Fragment>
));


Input.propTypes = {
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default Input;
