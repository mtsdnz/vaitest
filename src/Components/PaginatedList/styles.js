import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  loading: {
    alignSelf: 'center',
    marginTop: 20,
    width: 80,
    height: 80,
  },
  fetchLoading: {
    flex: 1,
    alignSelf: 'center',
    width: 80,
    height: 80,
    marginTop: 5,
  },
  footerText: {
    alignSelf: 'center',
    color: colors.text.primary,
    marginTop: 15,
    marginBottom: 23,
  },
  retryIcon: {
    marginTop: 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
});
