import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import colors from '../../styles/colors';
import Icon from '../Icon/Icon';

export default class PaginatedListView extends Component {
  constructor(props) {
    super(props);
    this.state = { isRefreshing: false, page: props.startPage };
  }

  shouldComponentUpdate(nextProps, nextState, nextContext): boolean {
    const { error, items, isFetching } = this.props;
    const itemsChanged = items.length !== nextProps.items.length;
    const isFetchingChanged = isFetching !== nextProps.isFetching;
    const errorChanged = error !== nextProps.error;


    return itemsChanged
      || isFetchingChanged
      || errorChanged;
  }

  componentDidUpdate(prevProps): void {
    const { isRefreshing } = this.state;
    if (this.props.isFetching !== prevProps.isFetching && isRefreshing) {
      this.setState({ isRefreshing: false });
    }
  }


  renderFooter = () => {
    const { page } = this.state;
    const {
      canFetchMore, error, items, emptyMessage, endReachedMessage, isFetching,
    } = this.props;

    if (error) {
      return (
        <Icon
          name="undo"
          size={40}
          color={colors.primary}
          onPress={() => !isFetching && this.changePage(page)}
          style={styles.retryIcon}
        />
      );
    }

    if (isFetching) return <ActivityIndicator color={colors.primary} size="large" style={styles.fetchLoading} />;

    return !canFetchMore ? (
      <Text style={styles.footerText}>
        {!items || items.length === 0
          ? emptyMessage || ''
          : endReachedMessage || ''}
      </Text>
    )
      : <View />;
  };


  nextPage = (reset = false) => {
    const { page } = this.state;
    const {
      canFetchMore, isFetching, startPage, error,
    } = this.props;


    if (reset) {
      this.changePage(startPage);
    } else if (canFetchMore === true && isFetching === false && error === false) {
      this.changePage(page + 1);
    }
  };

  changePage = (newPage) => {
    const { pageChanged, startPage } = this.props;
    this.setState({
      page: newPage,
      isRefreshing: newPage === startPage ? true : this.state.isRefreshing,
    });
    if (typeof pageChanged === 'function') {
      pageChanged(newPage);
    }
  };

  refresh = () => {
    if (this.state.isRefreshing === false) this.nextPage(true);
  };

  getStateAndHelpers = () => {
    const {
      canFetchMore, error, items, isFetching,
    } = this.props;
    const { isRefreshing } = this.state;
    return {
      refresh: this.refresh,
      nextPage: this.nextPage,
      renderFooter: this.renderFooter,
      canFetchMore,
      refreshing: isRefreshing,
      error,
      items,
      endReached: distance => !isFetching && canFetchMore === true && this.nextPage(),
    };
  };


  render() {
    const {
      children, loading, isFetching, items,
    } = this.props;
    return (isFetching && items.length === 0 ? loading()
      : children(this.getStateAndHelpers()));
  }
}

PaginatedListView.defaultProps = {
  startPage: 1,
  canFetchMore: true,
  isFetching: false,
  error: false,
  loading: () => <ActivityIndicator size="large" color={colors.primary} />,
  emptyMessage: 'Não há items.',
  endReachedMessage: 'Não há mais items.',
  errorMessage: 'Erro no carregamento de dados. Por favor, verifique a sua conexão.',
};

PaginatedListView.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any),
  startPage: PropTypes.number,
  pageChanged: PropTypes.func.isRequired,
  canFetchMore: PropTypes.bool,
  isFetching: PropTypes.bool,
  error: PropTypes.bool,
  emptyMessage: PropTypes.string,
  endReachedMessage: PropTypes.string,
  errorMessage: PropTypes.string,
};
