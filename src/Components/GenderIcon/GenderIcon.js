import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon/Icon';

const GenderIcon = ({
  isMale, maleIcon, femaleIcon, maleColor, femaleColor, style, ...rest
}) => (
  <Icon
    name={isMale ? maleIcon : femaleIcon}
    style={[{ color: isMale ? maleColor : femaleColor }, style]}
    {...rest}
  />
);

GenderIcon.defaultProps = {
  maleIcon: 'mars',
  femaleIcon: 'venus',
  maleColor: '#0195da',
  femaleColor: '#804098',
};

GenderIcon.propTypes = {
  isMale: PropTypes.bool.isRequired,
  maleIcon: PropTypes.string,
  femaleIcon: PropTypes.string,
  maleColor: PropTypes.string,
  femaleColor: PropTypes.string,
};

export default GenderIcon;
