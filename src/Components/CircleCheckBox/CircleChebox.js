import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image } from 'react-native';
import circle from '../../assets/images/circle.png';
import circleFill from '../../assets/images/circle_fill.png';

const CircleCheckbox = ({
  onPress, checked, size, style, children,
}) => (
  <TouchableOpacity onPress={onPress} style={style}>
    <Image
      source={checked ? circleFill : circle}
      style={{ width: size, height: size, marginRight: 5 }}
    />
    {children}
  </TouchableOpacity>
);

CircleCheckbox.defaultProps = {
  size: 25,
};

CircleCheckbox.propTypes = {
  size: PropTypes.number,
  onPress: PropTypes.func,
  checked: PropTypes.bool,
};

export default CircleCheckbox;
