import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Icon from '../Icon/Icon';

class Sidebar extends Component {
  onMenuClick = ({ onPress, screenName }) => {
    const { navigation } = this.props;
    navigation.closeDrawer();

    if (typeof onPress === 'function') onPress(navigation);
    else if (screenName) navigation.navigate(screenName);
  };

  render() {
    const { menus } = this.props;

    const MenuItem = (props) => {
      const {
        title, icon,
      } = props;
      return (
        <TouchableOpacity
          style={styles.menuItemContainer}
          onPress={() => this.onMenuClick(props)}
        >
          {icon && <Icon style={styles.menuItemIcon} name={icon} />}
          <Text style={styles.menuItemText}>{title}</Text>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        {Array.isArray(menus) && menus.map(props => <MenuItem key={props.title} {...props} />)}
      </View>
    );
  }
}

Sidebar.propTypes = {
  menus: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
    onPress: PropTypes.func,
    screenName: PropTypes.string,
  })).isRequired,
};

export default Sidebar;
