import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  container: {
    margin: 15,
  },
  menuItemContainer: {
    height: 40,
    marginBottom: 15,
    width: '100%',
    flexDirection: 'row',
    borderBottomColor: colors.primary,
    borderBottomWidth: 1,
    alignItems: 'center',
  },

  menuItemIcon: {
    fontSize: 25,
    marginRight: 15,
  },

  menuItemText: {
    fontSize: 17,
  },
});
