import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const Icon = ({ type, ...rest }) => {
  let IconComponent;
  switch (type) {
    case 'MaterialIcons': {
      IconComponent = MaterialIcon;
      break;
    }
    default: {
      // Font padrão é a fontAwesome.
      IconComponent = FontAwesome;
      break;
    }
  }

  return <IconComponent {...rest} />;
};

Icon.defaultProps = {
  type: 'FontAwesome',
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  // Font do ícone (FontAwesome, MaterialIcons, etc.). Padrão é FontAwesome.
  type: PropTypes.oneOf(['FontAwesome', 'MaterialIcons']),
  size: PropTypes.number,
  color: PropTypes.string,
};

export default Icon;
