import React, { Component } from 'react';
import {
  View, Animated,
} from 'react-native';
import styles from './styles';
import Input from '../Input/Input';
import colors from '../../styles/colors';

class FloatingLabelInput extends Component {
  constructor(props) {
    super(props);

    this.labelAnimation = new Animated.Value(props.value === '' ? 0 : 1);
    this.state = { isFocused: false };
  }

  componentDidUpdate() {
    const { isFocused } = this.state;
    const { value } = this.props;

    Animated.timing(this.labelAnimation, {
      toValue: (isFocused || value !== '') ? 1 : 0,
      duration: 150,
    }).start();
  }

  handleFocus = (e) => {
    const { onFocus } = this.props;
    if (onFocus) onFocus(e);
    return this.setState({ isFocused: true });
  };

  handleBlur = (e) => {
    const { onBlur } = this.props;
    if (onBlur) {
      onBlur(e);
    }
    return this.setState({ isFocused: false });
  };

  render() {
    const {
      placeholder, error, style, inputStyle, placeholderStyle, ...rest
    } = this.props;

    const textStyle = {
      position: 'absolute',
      left: 7,
      top: this.labelAnimation.interpolate({
        inputRange: [0, 2],
        outputRange: [18, 0],
      }),
      fontSize: this.labelAnimation.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 14],
      }),
      color: this.labelAnimation.interpolate({
        inputRange: [0, 1],
        outputRange: ['#aaa', colors.primary],
      }),
    };

    return (
      <View style={style}>
        <Animated.Text style={[textStyle, placeholderStyle]}>
          {placeholder}
        </Animated.Text>
        <Input
          {...rest}
          error={error}
          style={[styles.input, inputStyle, error && styles.error]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}


export default FloatingLabelInput;
