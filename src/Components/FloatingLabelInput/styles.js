import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

export default StyleSheet.create({
  input: {
    height: 45,
    fontSize: 16,
    color: colors.text.dark,
    paddingBottom: 0,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#555',
    textAlignVertical: 'bottom',
  },
  error: {
    borderBottomColor: 'red',
  },
});
