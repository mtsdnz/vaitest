import React from 'react';

import MainNavigator from './Screens/Navigation/MainNavigator';

const App = () => (
  <MainNavigator />
);
export default App;
