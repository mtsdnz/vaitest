export default {
  apiUrl: 'https://randomuser.me/api/',
  /*
    número de usuários por página, que a API irá chamar, na lista de usuários do dashboard.
    Em um ambiente real, essa informação ficaria somente no componente da lista de usuários,
    porém, como é necessário utilizar essa informação em outras telas, colocarei ela aqui.
   */
  usersPerPage: 10,
};
