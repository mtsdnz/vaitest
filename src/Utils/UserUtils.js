/*
  Transforma um objeto 'nome' em string.
 */
export function parseName(name, includeTitle = false) {
  if (typeof name === 'object') {
    const { title, first, last } = name;
    let parsedName = '';
    const appendToName = (value) => {
      if (value) {
        parsedName += `${value} `;
      }
    };

    if (includeTitle) appendToName(`${title}.`);
    appendToName(first);
    appendToName(last);

    return parsedName.trim();
  }

  return name;
}
