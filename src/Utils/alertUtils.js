import { AlertIOS, Platform, ToastAndroid } from 'react-native';

/*
* Shows a toast on Android, and a alert on iOS.
* title:string -  The alert title(iOS Only)
* message:string - The message to appear on Alert or Toast.
* duration: number, - Toast duration(Android Only)
* gravity: number, - Toast position(Android Only)
* xOffset: number, - Toast position x offset(Android Only)
* yOffset: number, - Toast position y offset(Android Only)
* */
export function showToastOrAlert(title, message, duration = ToastAndroid.LONG, gravity = ToastAndroid.BOTTOM,
  xOffset = 25, yOffset = 50) {
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravityAndOffset(
      message,
      duration,
      gravity,
      xOffset,
      yOffset,
    );
  } else {
    AlertIOS.alert(
      title,
      message,
    );
  }
}
