import _ from 'lodash';
/*
  Transforma um objeto com erros em formato de array, para string, concatenando com um separator.
  exemplo:
  {email: ['error1', 'error2']}
  será convertido para:
  {email: 'error1' + separator + 'error2'}
 */
export function parseErrors(errors, separator = '\n') {
  if (errors) {
    return _.transform(errors, (result, value, key) => {
      result[key] = value.join(separator);
    }, {});
  }
}