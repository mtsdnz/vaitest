import axios from 'axios';
import Constants from '../Common/Constants';

const instance = axios.create({
  baseURL: Constants.apiUrl,
  timeout: 10000,
});

export default instance;
