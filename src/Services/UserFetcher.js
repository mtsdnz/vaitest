import { FetchBuilderPagination } from './FetchBuilder';
import localization from '../Localization';
import Api from './API';

export default class UserFetcher extends FetchBuilderPagination {
  constructor() {
    super({ api: Api });
  }

  get baseUrl() {
    return `?nat=${localization.currentCountryCode}&seed=vaiFrontendTest`;
  }

  withFields(fields) {
    if (!Array.isArray(fields)) {
      console.error('Select property must be an array');
      return this;
    }

    if (fields.length <= 0) {
      console.error('Select array couldn\'t be empty!');
      return this;
    }

    return this.addKey('inc', fields.join(','));
  }
}
