export default class FetchBuilder {
  constructor({ api }) {
    this.API = api;
    this.keys = {};
    if (!api) {
      throw new Error("API Can't be null");
    }
    if (new.target === FetchBuilder) {
      throw new TypeError('Cannot construct FetchBuilder class directly');
    }
  }

  get baseUrl() {
    throw new Error('BaseUrl must be implemented');
  }

  reset() {
    this.keys = {};
  }

  isValid(field) {
    if (field || field === 0) {
      if (Array.isArray(field) && field.length > 0) {
        return true;
      }
      return field !== '';
    }
    return false;
  }

  addKey(key, value) {
    if (!key || !this.isValid(value)) {
      if (value) console.error(`Key: ${key} Is not valid: ${value}`);
      return this;
    }

    this.keys[key] = value;
    return this;
  }

  fetch() {
    const params = this.keys;
    return new Promise((resolve, reject) => {
      this.API.get(this.baseUrl, {
        params,
      })
        .then(res => resolve(res.data))
        .catch(err => reject(err.response.data));
    });
  }
}

export class FetchBuilderPagination extends FetchBuilder {
  constructor(props) {
    super(props);
    if (new.target === FetchBuilderPagination) {
      throw new TypeError('Cannot construct FetchBuilderPagination class directly');
    }
  }

  limit(limit) {
    if (typeof limit !== 'number' || limit <= 0) {
      console.error(`Limit must be greater than 0. Given value: ${limit}.`);
      return this;
    }
    return this.addKey('results', limit);
  }

  page(p) {
    if (typeof p !== 'number') {
      console.error(`Page must be a number. Given value: ${p}.`);
      return this;
    }

    return this.addKey('page', p);
  }
}
