import UserFetcher from './UserFetcher';

class UsersService {
  getUsers() {
    return new UserFetcher();
  }
}

const service = new UsersService();
export default service;
