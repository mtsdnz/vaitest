const baseColors = {
  primary: '#687089',
};

const colors = {
  text: {
    primary: baseColors.primary,
    light: '#fff',
    dark: '#000',
  },

  header: {
    background: baseColors.primary,
    text: '#fff',
    button: '#fff',
  },

  button: {
    background: baseColors.primary,
    text: '#fff',
  },

};

export default { ...baseColors, ...colors };
