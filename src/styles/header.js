import colors from './colors';

export default {
  defaultStyle: {
    headerStyle: {
      backgroundColor: colors.primary,
      color: colors.text.light,
      elevation: 1,
    },
    headerTintColor: colors.header.button,
    headerTitleStyle: {
      color: colors.header.text,
      fontWeight: 'bold',
      fontSize: 19,
      alignSelf: 'center',
    },
  },
  headerTransparent: {
    backgroundColor: colors.primary,
    color: colors.text.light,
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    elevation: 0,
  },
};
