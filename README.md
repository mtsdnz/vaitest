# Vai Test

Criado como solução do [FrontEND Test](https://github.com/blongg/vai-code-challenges/blob/master/FRONTEND.md)


# Stack
 - React 16.8.6
 - React-Native 0.60.4

# Setup

Instale o CLI do React-Native globalmente, com o comando:
```powershell
npm install -g react-native-cli
```
Depois instale as dependencias do projeto:
```powershell
npm install
```

Após instalar as dependências, instale o applicativo no Emulador, ou no seu celular (O Appfoi testado somente em **Android**):
```powershell
react-native run-android
```

Depois inicie o servidor de desenvolvimento: 
```powershell
react-native start
```
Inicie o aplicativo.

# Estrutura do Projeto 

### Assets
Pasta com os assets estáticos do applicativo.
### Common
Pasta contendo constantes que poderão ser utilizadas em vários lugares do App.
### Components
Contém todos os componentes de UI do aplicativo.
### Containers
Contém a **UI** das telas. 
### Localization
Contém constantes de localização (linguagem padrão, lista de linguagens disponíveis)
### Screens
 1. **Auth**: Contém as telas de autênticação;
2. **Dashboard**: Contém as telas do Dashboard
3. **Navigation**: Contém toda a estrutura e lógica de navegação (entre telas) do aplicativo.
4. **ScreenNames.js** : Arquivo contendo o nome de todas as telas do App.

### Services
Contém os serviços do aplicativo. É  a parte que faz todas as requisições no backend.

Para adquirir os dados da API, é utilizada o **UserFetcher**, que herda do **FetchBuilder**. FetchBuilder e UserFetcher são classes apenas para **obter dados**(GET), elas basicamente montam a URL, permitindo adicionar parâmetros de forma legível e fácil, como podem ver no código abaixo:


```javascript
UsersService  //Instância do serviço de usuário
.getUsers()  //Retorna uma instância da classe UserFetcher
 .withFields(['name', 'picture', 'gender', 'email', 'phone', 'location']) // especifica quais fields da API nós utilizaremos (parâmetro inc da url)  
 .page(1) // Número da página (parâmetro page)  
 .limit(12) // Quantidade de itens por página (parâmetro results)  
 .fetch()  //Faz o fetch com axios, e retorna uma Promise.
```

### Styles
Contém alguns estilos que serão utilizados globalmente no aplicativo.

### Utils
Contém helpers, que poderão ser utilizados globalmente no aplicativo.

# Bibliotecas Utilizadas (UI)

 - **react-native-datepicker**: Date Picker para android & IOS.
 - **react-native-fast-image**: Componente para carregar imagens, de maneira optmizada. Decidir usar esse componente, pois a nossa lista de usuários pode ficar bem grande, e com o **Image** nativo, a nossa perfomance pode ficar péssima, com muitas imagens.
 -  **react-native-image-picker**: Permite usar a interface nativa para selecionar uma foto da biblioteca do dispositivo ou diretamente da câmera.
 - **react-native-vector-icons**: Biblioteca de ícones.

# Melhorias
Algumas melhorias que poderiam ser feitas (caso houvesse mais tempo), são:

**InputComponent**:  Faria de uma maneira mais "customizável", isto é, com mais propriedades. 

**ButtonComponent**: Faria de uma maneira mais “customizável”, isto é, com mais propriedades.

**Sistema de Cores**: Estruturaria melhor o sistema de cores do app (colocaria mais variáveis, cores, etc) e criaria um sistema de Tema, utilizando o Provider, do React.

**Validações**: Adicionaria mais validações nos campos do Perfil do usuário.

**Testes**: Adicionaria testes com JEST.

# Interface
Segue algumas screenshots da interface do app:


![Login](https://bitbucket.org/mtsdnz/vaitest/raw/af9b04cd7d16347cff51f31197aa31b573a5cf9f/app-screenshots/login.jpeg) 
(Tela de Login)

![Inicio](https://bitbucket.org/mtsdnz/vaitest/raw/af9b04cd7d16347cff51f31197aa31b573a5cf9f/app-screenshots/home.jpeg) 
(Home)

![Sidebar](https://bitbucket.org/mtsdnz/vaitest/raw/af9b04cd7d16347cff51f31197aa31b573a5cf9f/app-screenshots/sidebar.jpeg)
(Sidebar)

![Perfil](https://bitbucket.org/mtsdnz/vaitest/raw/af9b04cd7d16347cff51f31197aa31b573a5cf9f/app-screenshots/profile.jpeg) 
(Perfil)